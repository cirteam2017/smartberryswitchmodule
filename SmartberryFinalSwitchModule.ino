#include "RFLibrary.h"

const int SWITCH_PIN = 4;

Packet* packetToSend(){
  return NULL;
  }

void packetReceived(Packet packet){
  Serial.println("Received packet");
  int startingIndex = 5;
  if(packet.isMirroring())
    startingIndex++;
    Serial.println(packet.content[startingIndex]);
    Serial.println(packet.content[startingIndex + 1]);
  switch(packet.content[startingIndex]){
    case 0x30:
      if(packet.content[startingIndex + 1]){
        Serial.println("Writing LOW");
        digitalWrite(SWITCH_PIN,LOW);
        }else{
          digitalWrite(SWITCH_PIN,HIGH);
          }
    }
  }

RFLibrary rfLibrary = RFLibrary(1000,false,packetToSend,packetReceived);
void setup() {
  pinMode(SWITCH_PIN, OUTPUT);
  rfLibrary.setupRFModule(5,2,1,1);
  
}

void loop() {
  digitalWrite(SWITCH_PIN,HIGH);
  
  rfLibrary.startLoop();

}
